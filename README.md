-----

## Overview

The **stats4dthemes** package is a fork of the youngmetro R package. 
youngmetro is a semi-custom beamer theme for use in
RStudio via R Markdown. This theme borrows heavily from the
[HSRM](https://github.com/benjamin-weiss/hsrmbeamertheme),
[sthlm](https://github.com/markolsonse/sthlmBeamerTheme), and
[Metropolis](https://github.com/matze/mtheme) Beamer themes.

## Installation 🔌

``` r
if (!require("devtools")) {
  install.packages("devtools", dependencies = TRUE) 
}
devtools::install_git("https://gitlab.com/stats4d/stats4dthemes")
```

## Usage 💻

### RStudio

The `stats4dthemes` theme is designed to be used primarily within RStudio.
Once installed, select *File* \> *New File* \> *R Markdown* \> *From
Template* \> `stats4dthemes`. After naming and selecting a location for
your prestnation, a new R Markdown document will open in the Source
pane. This document contains some basic information that you will want
to update (see **Front Matter**
below).

<img src="http://aaronbaggett.com/images/from_template.png" align="center" />

### R

The following should work if you would like to use `stats4dthemes` outside of RStudio:

``` r
rmarkdown::draft("slide_deck.Rmd", template = "metro_beamer", package = "stats4dthemes")

rmarkdown::render("slide_deck.Rmd")
```

### Front Matter

The Pandoc YAML is pre-populated with the following:

``` yaml
---
title: "Presentation Title"
shorttitle: "Short Title"
subtitle: "Presentation Subtitle"
author: "Ahmadou Dicko, Ph.D."
date: '`r format(Sys.Date(), "%B %d, %Y")`'
short-date: '`r format(Sys.Date(), "%m/%d/%y")`'
institute: Statistics for development
short-institute: "STATS4D"
department: "STATS4D"
mainfont: IBM Plex Sans
monofont: IBM Plex Mono
fontsize: 14pt
classoption: aspectratio = 1610
output: 
   stats4dthemes::metro_beamer
---
```

